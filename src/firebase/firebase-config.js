import { initializeApp } from "firebase/app";
import { getToken, onMessage, getMessaging } from "firebase/messaging";

// Confirguracion para inicializar el firebase

const firebaseApp = initializeApp({
    apiKey: "AIzaSyDtI2Zf_0jAYq9TYrgDdAk3iAjwMNkJS0I",
    authDomain: "push-notification-ee786.firebaseapp.com",
    projectId: "push-notification-ee786",
    storageBucket: "push-notification-ee786.appspot.com",
    messagingSenderId: "258325717630",
    appId: "1:258325717630:web:66687c0d9adf977d0fae2f",
    measurementId: "G-JM5PE4FBD5"
});

// Configuracion de Cloud messaging para recibir mensajes de notificacion
const messaging = getMessaging(firebaseApp)

// Configuracion credenciales web - token para mensajes

const {REACT_APP_VAPID_KEY} = process.env
const publicKey = REACT_APP_VAPID_KEY

// Configuracion para acceder al token de registro

export const getTokenFirebase = async()=>{
    let currentToken = ''

    try {
        currentToken = await getToken(messaging, {vapidKey:publicKey})
        if(currentToken){
            console.warn('success token',currentToken)
        } else {
            console.warn('failed token',currentToken)
        }
    } catch (error) {
        console.log('Ocurrio un error obteniendo el token', error)
    }
    return currentToken
}
// On Message
export const firstPlaneMessage = onMessage(messaging, (payload)=>{
    console.log('Message received. ', payload)
})
export const onMessageListener = () =>
    new Promise((resolve) => {
    messaging.onMessage((payload) => {
    resolve(payload);
    });
});
