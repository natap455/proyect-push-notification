//importScripts('https://www.gstatic.com/firebasejs/8.1.2/firebase-app.js');
//importScripts('https://www.gstatic.com/firebasejs/8.1.2/firebase-messaging.js');

importScripts('/__/firebase/9.1.1/firebase-app-compat.js');
importScripts('/__/firebase/9.1.1/firebase-messaging-compat.js');
importScripts('/__/firebase/init.js');

const firebaseConfig = {

  apiKey: "AIzaSyDtI2Zf_0jAYq9TYrgDdAk3iAjwMNkJS0I",
  authDomain: "push-notification-ee786.firebaseapp.com",
  projectId: "push-notification-ee786",
  storageBucket: "push-notification-ee786.appspot.com",
  messagingSenderId: "258325717630",
  appId: "1:258325717630:web:66687c0d9adf977d0fae2f",
  measurementId: "G-JM5PE4FBD5"
};

firebase.initializeApp(firebaseConfig);

// Configuracion para crear notificaciones con campos de título, ícono y cuerpo.

const messaging = firebase.messaging();

messaging.onBackgroundMessage(function(payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  const notificationTitle = payload.notification.title;
  const notificationOptions = {
    body: payload.notification.body,
    icon: payload.notification.icon
    //icon: '/icon.png'
  };

  self.registration.showNotification(notificationTitle,
    notificationOptions);
});
